    .include "p30F4013.inc"
    
    .GLOBAL	__T1Interrupt;
    .GLOBAL	_habilitarCLK_EXT;
    .GLOBAL	_DHOR;
    .GLOBAL	_UHOR;
    .GLOBAL	_DMIN;
    .GLOBAL	_UMIN;
    .GLOBAL	_DSEG;
    .GLOBAL	_USEG;
    
__T1Interrupt:
    PUSH.S;
    
    INC.B		_USEG;
    MOV			#10,	    W0;
    CP.B		_USEG;
    BRA			NZ,	    FIN;
    CLR			_USEG;
    INC.B		_DSEG;
    MOV			#6,	    W0;
    CP.B		_DSEG;
    BRA			NZ,	    FIN;
    CLR			_DSEG;
    INC.B		_UMIN;
    MOV			#10,	    W0;
    CP.B		_UMIN;
    BRA			NZ,	    FIN;
    CLR			_UMIN;
    INC.B		_DMIN;
    MOV			#6,	    W0;
    CP.B		_DMIN;
    BRA			NZ,	    FIN;
    
    CLR			_DMIN;
    INC.B		_UHOR;
    
    MOV			#2,	    W0;
    CP.B		_DHOR;
    BRA			NZ,	    NO2;
    MOV			#4,	    W0;
    CP.B		_UHOR;
    BRA			NZ,	    FIN;
    CLR			_UHOR;
    CLR			_DHOR;
    GOTO		FIN;
    
NO2:
    MOV			#10,	    W0;
    CP.B		_UHOR;
    BRA			NZ,	    FIN;
    CLR			_UHOR;
    INC.B		_DHOR;
    
    
FIN:
    BCLR		IFS0,	    #T1IF;
    POP.S;
    
    retfie;
    
_habilitarCLK_EXT:
    PUSH    W0;
    PUSH    W1;
    PUSH    W2
    
    MOV	    #OSCCON,		W0;
    MOV	    #0x46,		W1;
    MOV	    #0x57,		W2;
    
    MOV.B   W1,			[W0];
    MOV.B   W2,			[W0];
    
    BSET    OSCCON,		#LPOSCEN;
    
    POP	    W2;
    POP	    W1;
    POP	    W0;
    
    RETURN;
    