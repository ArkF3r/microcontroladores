    .include "p30F4013.inc"
    
    .GLOBAL	__INT0Interrupt;
    .GLOBAL	_UMI;
    .GLOBAL	_CEN;
    .GLOBAL	_DEC;
    .GLOBAL	_UNI;
    
__INT0Interrupt:
    PUSH.S;
    INC.B		_UNI;
    MOV			#10,	    W0;
    CP.B		_UNI;
    BRA			NZ,	    FIN;
    CLR			_UNI;
    INC.B		_DEC;
    CP.B		_DEC;
    BRA			NZ,	    FIN;
    CLR			_DEC;
    INC.B		_CEN;
    CP.B		_CEN;
    BRA			NZ,	    FIN;
    CLR			_CEN;
    INC.B		_UMI;
    CP.B		_UMI;
    BRA			NZ,	    FIN;
    CLR			_UMI;
    
FIN:
    BCLR		IFS0,	    #INT0IF;
    POP.S;
    
    retfie;
    
    




