import serial
with serial.Serial('COM3', 115200) as ser:
    print(ser.name)         # check which port was really used
    while True:
        a = ser.read() 
        try:    print(a.decode('utf-8'), end='')
        except: pass
    ser.close() 