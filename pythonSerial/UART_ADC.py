import time
import serial
import numpy as np
from matplotlib import pyplot as plt
print("Ingrese el voltaje de alimentación")
voltaje = float(input("V= "))
ser = serial.Serial('COM3', 115200)  # open serial port
print(ser.name)         # check which port was really used
muestras = []
i = 0
prenumber = ""
while i<4096:
    a = ser.read()
    binary = int.from_bytes(a, byteorder='little',signed=False)
    binary = '{0:08b}'.format(binary)
    control = binary[:2]
    binary = binary[2:]
    if control[0] == '1':
        if len(prenumber) == 6:
            prenumber = binary + prenumber
            muestras.append(int(prenumber, 2)* voltaje/4096)
            i += 1
        prenumber = ""
    else:
        prenumber = binary
        
ser.close()
print("Termino muestreo")
np.savetxt('muestrasADC.txt', muestras, delimiter=',', header="Muestras de ADC del microcontrolador DSP30F4013 ({})".format(time.ctime()), fmt='%i')
muestras = np.array(muestras)
fig, ax = plt.subplots()
ax.plot(muestras)
plt.yticks(np.arange(0,voltaje + (voltaje/20) ,step=voltaje/20))
fig.canvas.manager.full_screen_toggle()
plt.show()