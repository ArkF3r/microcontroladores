    .include "p30F4013.inc"
    
    .GLOBAL _datoLCD
    .GLOBAL _comandoLCD
    .GLOBAL _busyFlagLCD
    .GLOBAL _iniLCD8bits
    .GLOBAL __U1RXInterrupt
    
    __U1RXInterrupt:
	PUSH	W0;
	
	CALL	_busyFlagLCD
	MOV	U1RXREG,    W0;
	    
	MOV	#10,	    W1;
	CP	W0,	    W1;
	BRA	NZ,	    NOENTER;
	    
	CALL	_busyFlagLCD;
	MOV	#1,	    W0;
	CALL	_comandoLCD;
	GOTO	FIN;
	    
    NOENTER:
	CALL	_datoLCD;
		
    FIN:
	BCLR	IFS0,	    #U1RXIF;
	POP	W0;
    RETFIE;
