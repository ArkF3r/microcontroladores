        .include "p30F4013.inc"

    .GLOBAL _comandoLCD;
    .GLOBAL _datoLCD;
    .GLOBAL _busyFlagLCD;
    .GLOBAL _iniLCD8bits;
    .GLOBAL _escribeLCD;

    .EQU    RS_LCD, RD3;
    .EQU    RW_LCD, RD9;
    .EQU    EN_LCD, RD2;
    .EQU    BF_LCD, RB7;
    
_iniLCD8bits:
    PUSH    W0;
    
    CALL    _RETARDO_15ms
    MOV	    #0X30,   W0;
    CALL    _comandoLCD;
    CALL    _RETARDO_15ms
    MOV	    #0X30,   W0;
    CALL    _comandoLCD;
    CALL    _RETARDO_15ms
    MOV	    #0X30,   W0;
    CALL    _comandoLCD;
    
    CALL    _busyFlagLCD;
    MOV	    #0X38,   W0;
    CALL    _comandoLCD;
    CALL    _busyFlagLCD;
    MOV	    #0X08,   W0;
    CALL    _comandoLCD;
    CALL    _busyFlagLCD;
    MOV	    #0X01,   W0;
    CALL    _comandoLCD;
    CALL    _busyFlagLCD;
    MOV	    #0X06,   W0;
    CALL    _comandoLCD;
    CALL    _busyFlagLCD;
    MOV	    #0X0F,   W0;
    CALL    _comandoLCD;
    
    POP	    W0;
    return;

_busyFlagLCD:
    
    PUSH	W0;
    PUSH	W1;
    
    SETM.B	TRISB;
    NOP;
    
    BCLR	PORTD,	    #RS_LCD;
    NOP;
    
    BSET	PORTD,	    #RW_LCD;
    NOP;
    
    BSET	PORTD,	    #EN_LCD;
    NOP;
    
    CHECK_FLAG:
    BTSC	PORTB,	    #BF_LCD;
    GOTO	CHECK_FLAG;
    
    BCLR    PORTD,	    #EN_LCD;
    NOP;
    
    BCLR    PORTD,	    #RW_LCD;
    NOP;
    
    CLR.B	    TRISB;
    NOP;
    
    POP	    W1;
    POP	    W0;
    return;

_datoLCD:
    
    BSET	PORTD,	    #RS_LCD;
    NOP;
    BCLR	PORTD,	    #RW_LCD;
    NOP;
    BSET	PORTD,	    #EN_LCD;
    NOP;
    MOV.B	WREG,	    PORTB;
    NOP;
    BCLR	PORTD,	    #EN_LCD;
    NOP;
    
    return;
    
_comandoLCD:
    
    BCLR	PORTD,	    #RS_LCD;
    NOP;
    BCLR	PORTD,	    #RW_LCD;
    NOP;
    BSET	PORTD,	    #EN_LCD;
    NOP;
    MOV.B	WREG,	    PORTB;
    NOP;
    BCLR	PORTD,	    #EN_LCD;
    NOP;
    
    return
    
_escribeLCD:
    
    PUSH	W1;
    MOV		W0,	    W1
    CLR		W0;
CICLOCAD:
    MOV.B	[W1++],	    W0;
    CP0		W0;
    BRA		Z,	    FINCAD
    CALL	_datoLCD;
    CALL	_busyFlagLCD;
    GOTO	CICLOCAD;
FINCAD:
    POP		W1;
    RETURN;
    
    
_RETARDO_15ms:
    PUSH    W0;
    MOV	    #0x360D,    W0;
    CICLO_15ms:
    DEC	    W0,	    W0;
    BRA	    NZ,	    CICLO_15ms;
    
    POP	    W0;
    
    RETURN;
    