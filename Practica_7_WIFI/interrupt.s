    .include "p30F4013.inc"
    
    .GLOBAL __U1RXInterrupt;
    
    .GLOBAL _comando;

    .GLOBAL _retardo1S;

    __U1RXInterrupt:
	push w0
	MOV	    U1RXREG,	    W0;
	MOV	    W0,		    U2TXREG;
	BCLR IFS0, #U1RXIF;
	pop w0
    retfie
    
    _comando:
	PUSH	W1;
	MOV		W0,	    W1
	CLR		W0;
    CICLOCAD:
        MOV.B	[W1++],	    W0;
	CP0		W0;
	BRA		Z,	    FINCAD
	BCLR		IFS0,	    #U1TXIF;
	MOV		W0,	    U1TXREG;
	CHECATXIF:
	BTSS		IFS0,	    #U1TXIF;
	GOTO		CHECATXIF;
	GOTO	CICLOCAD;
    FINCAD:
        POP		W1;
	RETURN;

    _retardo1S:
    PUSH W0;
    PUSH W1;
    MOV	    #10,    W1;
    ;MOV	    #2,    W1;
    CICLO2_1S:
	;MOV	    #2,    W0;
	CLR	    W0;
    CICLO1_1S:
	DEC	    W0,	    W0;
	BRA	    NZ,	    CICLO1_1S; 
	DEC	    W1,	    W1
	BRA	    NZ,	    CICLO2_1S; 
    POP W1;
    POP W0;
    RETURN

