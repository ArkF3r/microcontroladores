    .include "p30F4013.inc"
    
    .GLOBAL __ADCInterrupt;
    .GLOBAL __T3Interrupt;
    
    __T3Interrupt:
	BTG	LATD,	#LATD0;
	BCLR	IFS0,	#T3IF;
    RETFIE;
	
    __ADCInterrupt:
	PUSH	W0;
	PUSH	W1;
	MOV	ADCBUF0,	W0;
	LSR	W0,	#6,	W1;
	BSET	W1,		#7;
	AND	#15,		W0;
	MOV	W0,		U1TXREG;
	MOV	W1,		U1TXREG;
	BCLR	IFS0,		#ADIF;
	POP	W1;
	POP	W0;
    RETFIE;
    